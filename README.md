### Hey! My name is Odin 👋

## I'm a Graduate Trainee at Experis Academy, on my way to becoming a fullstack .NET-developer!
- 👀 I’m interested in programming, exercise, video games and stocks!
- 🌱 I’m currently learning C#, React JS, Angular, Docker and distribution through Microsoft Azure.

<br />

### Languages and Tools: 

<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/java/java.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/cpp/cpp.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/vue/vue.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/mysql/mysql.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/git/git.png"></code>


![Aryclenio GitHub Stats](https://github-readme-stats.vercel.app/api?username=odinkvarving&show_icons=true&theme=monokai)
